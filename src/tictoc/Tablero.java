/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictoc;

/**
 *
 * @author davidpm
 */
public class Tablero {
    
    char tablero[][];
    
    public Tablero(){
        
        this.tablero = new char[3][3];
        limpiarTablero();
    }
    
    private void limpiarTablero(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                this.tablero[i][j] = ' ';
            }
        }
    }
    public void nuevaPartida(){
        limpiarTablero();
    }
    
    
    public void getTablero(){
        
        System.out.println("Tres en raya:");
        for(int i = 0; i < 3; i++){
            System.out.print(" " + (3 - i));
            for(int j = 0; j < 3; j++){
                if(j==2){
                System.out.print("|" + this.tablero[i][j] + "|");
                }else{
                System.out.print("|" + this.tablero[i][j]);
                }
            }
            System.out.println("");  
        }
            System.out.println("   a b c "); 
    }
    
}
